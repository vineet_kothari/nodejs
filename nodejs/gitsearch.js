#!/home/kothari/.nvm/versions/node/v6.1.0/bin/node
//shebag it is used to show the path of interpreter
//use which node (also remember to do sudo npm install -g as it will update any changes done to this file)
//use which gitsearch if result is blank irt can be used as cxommand line utility 
//console.log(process.argv);//Options and arguments passed into a command can be accessed via process.argv
var program = require('commander');
var request = require('request'); //As the GitHub api uses HTTP endpoints 
//mentioning jquery and ordered by star count using chalk to minify.s json data will be returned
var chalk = require('chalk');
//command line arguments
program
    .version('0.0.1')
    .usage('[options] <keywords>')
    .option('-o, --owner [name]', 'Filter by the repositories owner')
    .option('-l, --language [language]', 'Filter by the repositories language')
    .parse(process.argv);

if(!program.args.length) {
    program.help();
} else {
    var keywords = program.args;

    var url = 'https://api.github.com/search/repositories?sort=stars&order=desc&q='+keywords;

    if(program.owner) {
        url = url + '+user:' + program.owner;
    }

    if(program.language) {
        url = url + '+language:' + program.language;
    }

}
// that Github’s api requires all requests to have a valid User-Agent header
request({
    method: 'GET',
    headers: {
        'User-Agent': 'vinit012345'
    },
    url: url
}, function(error, response, body) {

    if (!error && response.statusCode == 200) {
        var body = JSON.parse(body);
        console.log(body);
    } else if (error) {
        console.log('Error: ' + error);
    }

//In the end check for errors
if (!error && response.statusCode == 200) {
    var body = JSON.parse(body);

    for(var i = 0; i < body.items.length; i++) {
        console.log(chalk.cyan.bold.underline('Name: ' + body.items[i].name));
        console.log(chalk.magenta.bold('Owner: ' + body.items[i].owner.login));
        console.log(chalk.grey('Desc: ' + body.items[i].description + '\n'));
        console.log(chalk.grey('Clone url: ' + body.items[i].clone_url + '\n'));
    }
} else if (error) {
    console.log(chalk.red('Error: ' + error));
}
});
