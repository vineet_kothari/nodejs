## Command Line Utility Script Example Nodejs Ubuntu/linux Type Os##
This README would normally document whatever steps are necessary to get your application up and running.

* Quick summary:A basic command line utility in nodejs to search git from command line (linux/ubuntu)
* Version : 0.0.1

### How do I get set up? 

**Install nodejs**  *sudo apt-get install nodejs* 

**Install npm** *node install npm.*

### Configuration 

**Create package.json** npm init . 

### Dependencies  

**request** *npm install request --save*

**chalk(pretty print json)** *npm install chalk --save*

**commander (command line uility)** *npm install commander --save*